\section{Konstrukcja grafów zachowujących niski indeks jednomianowy}

Teraz poznamy kilka twierdzeń i lematów, które pomogą nam w tworzeniu grafów z
niskim indeksem jednomianowym.

\begin{lem}\label{lem:kombinacja-liniowa}
Niech $A$ i $L$ będą kwadratowymi macierzami rozmiaru $n$, takimi, że każda
kolumna $L$ jest liniową kombinacją kolumn macierzy $A$. Niech $n_j$ będzie
liczbą tych kolumn macierzy $L$, w których $j$-ta kolumna macierzy $A$ występuje
z niezerowym współczynnikiem. \\
Jeśli dla każdego $j$ zachodzi nierówność $n_j \leqslant r $ oraz $\per(L)
\neq 0 $, wtedy $\pind(A) \leqslant r$.
\end{lem}
\begin{proof}
Z założenia wiemy, że dla każdego $k \in \{1,2,\ldots,n\}$, $k$-tą kolumnę $L_k$
możemy przedstawić jako:
$$L_k= \sum_{j_k=1}^n \alpha_{kj_k} A_{j_k}.$$
Z równoważnej definicji permanentu otrzymujemy:
\begin{align*}
\per(L)=& \per(L_1,L_2,\ldots,L_n)=\\
&=\per(\sum_{j_1=1}^n \alpha_{kj_1}
A_{j_1},\sum_{j_2=1}^n \alpha_{kj_2} A_{j_2},\ldots,\sum_{j_n=1}^n
\alpha_{kj_n} A_{j_n}),
\end{align*}
z kolei z liniowości otrzymujemy zatem:
\begin{align*} 
\per(L)&=\sum_{j_1=1}^n \alpha_{kj_1} \sum_{j_2=1}^n \alpha_{kj_2} \dotsm
\sum_{j_n=1}^n \alpha_{kj_n} \cdot \per(
A_{j_1}, A_{j_2},\ldots, A_{j_n})
\end{align*}
Jeśli $\per(L) \neq 0$, to jeden ze składników sumy jest różny od 0. Jeśli $n_j
\leqslant r$, to wiemy że w każdym ze składników sumy kolumny powtarzają się
co najwyżej $r$ razy. Stąd $\pind(A) \leqslant r$. 
\end{proof}

W dalszych rozważaniach przydatna będzie następująca macierz $S$.
\begin{df}
Niech $S=(s_{ij})$ będzie macierzą kwadratową wymiaru $2n+1$, dla $n\geqslant1$,
zdefiniowaną w następujący sposób:
$$
 s_{ij} =   \begin{cases}
     0  &  \mathrm{jeśli}\ i=1 \mbox{ oraz }\ j\in\{1,2\}
     \\
     -1  &  \mathrm{jeśli}\ i \ \mbox{jest nieparzyste, } i\geqslant 3 \mbox{
     oraz }\ j\in\{1,2\}
      \\
     1  &  \mbox{w pozostałych przypadkach.}
  \end{cases} 
$$
\end{df}
Dla $n=2$, wygląda ona następująco:
$$
S = \begin{bmatrix}
0 & 0 & 1 & 1 & 1 \\ 
1 & 1 & 1 & 1 & 1\\ 
-1 & -1 & 1 & 1 & 1\\ 
1 & 1 & 1 & 1 & 1\\ 
-1 & -1 & 1 & 1 & 1
\end{bmatrix}
 $$

 Udowodnimy następujący lemat:
\begin{lem}\label{lem:perS}
Dla każdego $n \geqslant 1$, $\per(S)= -(2n)!$
\end{lem}
\begin{proof}
Zauważmy, że permanent macierzy $S$ możemy zapisać w postaci:
$$\per(S)=\sum_{i=1}^{2n+1} a_{i1}\cdot S_i ,$$
gdzie $S_i$ jest to macierz powstała z macierzy $S$ poprzez usunięcie pierwszej
kolumny oraz $i$-tego wiersza\footnote{ Prezentowana suma jest analogiczna jak w
przypadku rozwinięcia Laplace'a (względem pierwszej kolumny) wyznacznika macierzy.}.

Przydatna będzie następująca obserwacja:
\begin{obs}\label{obs:transpozycja}
Niech $A$ będzie macierzą kwadratową wymiaru $n$ oraz niech $A'$ będzie macierzą
powstałą z $A$ poprzez transpozycję dwóch kolumn. Wtedy $\per(A)=\per(A')$.
\end{obs}
\begin{proof}
Dla ustalenia uwagi załóżmy, że transpozycja dotyczyła wierszy $i$ oraz $j$,
gdzie $i\neq j$. Aby udowodnić równość z tezy, przyjrzyjmy się
dowolnemu składnikowi z sumy:
$$ \per(A) = \sum_{\sigma \in S_n}
a_{1\sigma(1)}a_{2\sigma(2)}\dotsm a_{n\sigma(n)} .$$
Ponieważ sumujemy po wszystkich permutacjach, to dla dowolnego ustalonego
składnika z lewej strony równości
$$\per(A)=\per(A') ,$$
 odpowiadającego permutacji $\sigma_0$,
istnieje składnik jemu równy po prawej stronie dowodzonej równości - jest to permutacja
$\sigma_0'$ zdefiniowana w następujący sposób: $$
 \sigma_0'(k) =   \begin{cases}
     \sigma(k)  &  \mathrm{jeśli}\ k \notin \{i,j\},\ 
     \\
     \sigma(j)  &  \mathrm{jeśli \ } k=i,
      \\
     \sigma(i)  &  \mathrm{jeśli}\ k=j
  \end{cases} 
.$$ Oczywiście jest to również permutacja. Ponadto takie przyporządkowanie
permutacji jest wzajemnie jednoznaczne. Kończy to dowód tej obserwacji.
\end{proof}
Wracając teraz do dowodu lematu, mamy:
\begin{align*}
\per(S)=&\sum_{i=1}^n \per(S_{2i}) - \sum_{i=1}^n \per(S_{2i+1}).
\end{align*}
Z kolei dzięki obserwacji \ref{obs:transpozycja} otrzymujemy zatem:
\begin{equation}\label{eq:per(S)}
\per(S)=n\cdot \per(A) - n\cdot \per(B),
\end{equation}
gdzie:
$$
A = \begin{bmatrix}
0 & 1 & \dotsm & 1 \\ 
1 & 1& \dotsm  & 1\\
\vdots & \vdots & \ddots & \vdots \\
1 & 1& \dotsm  & 1\\
-1 & 1& \dotsm  & 1\\
\vdots & \vdots & \ddots & \vdots \\
-1 & 1& \dotsm  & 1\\
\end{bmatrix},
 $$
 przy czym wierszy mających $1$ w pierwszej kolumnie jest $n-1$, a tych z $-1$
 na pierwszym miejscu jest $n$.
 Macierz $B$ wygląda analogicznie, z tą różnicą, że wierszy z $1$ w pierwszej
 kolumnie jest $n$, zaś tych z $-1$ jest $n-1$.
 Korzystając ponownie z rozwinięcia względem pierwszej kolumny otrzymujemy:
 $$ \per(A)=(0\cdot 1 + (-1)\cdot n + 1\cdot(n-1))\cdot (2n-1)!=(-1)\cdot(2n-1)!
 $$
oraz
$$ \per(B)=(0\cdot 1 + (-1)\cdot (n-1) + 1\cdot n)\cdot
(2n-1)!=(2n-1)! .$$
Zatem wracając do równości (\ref{eq:per(S)}) otrzymujemy:
$$ \per(S)=n \cdot (-1)(2n-1)!- n\cdot (2n-1)!=-(2n)! .$$
\end{proof}