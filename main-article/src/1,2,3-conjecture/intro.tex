\section{Wstęp}

W tym rozdziale, bazując na pracy \cite{bartnicki-grytczuk} przedstawimy istotę
hipotezy 1-2-3 oraz zaprezentujemy pewne wyniki oraz narzędzia, dzięki którym
udało się je uzyskać. Wszystkie uzupełnienia dowodów, obserwacje, przykłady itp.
zawarte w tym rozdziale, które nie znajdują się w wyżej wymienionej publikacji, są mojego autorstwa (chyba że jest napisane inaczej).

\section{Podstawowe definicje} \label{sec:definicje-hip-123}
Na początku zdefiniujmy ważenie, które będziemy rozważać w tej części.

\begin{df}
Niech $G=(V,E)$ będzie grafem prostym. \textit{Ważeniem krawędzi} grafu $G$
przez zbiór $S \subset \mathbb{R}$, nazywamy odwzorowanie $w$: $$ w : E
\rightarrow S .$$
\end{df}

\begin{df}
Niech $G=(V,E)$ będzie grafem prostym, $S \subset \mathbb{R}$, a $w$, ważeniem krawędzi tego
grafu przez $S$.
\textit{Kolorem wierzchołka} $v\in V$ nazywamy sumę wag krawędzi incydentnych z
$v$, czyli liczbę $X_v = \sum_{e \in E_v} w(e)$, gdzie $E_v$ oznacza zbiór krawędzi
incydentnych z wierzchołkiem $v$.
\end{df}

\begin{df}
Niech $G$ będzie grafem prostym oraz niech $S$ będzie podzbiorem liczb
rzeczywistych. Mówimy, że $G$ jest \emph{wagowo kolorowalny} przez $S$, jeśli
istnieje ważenie krawędzi $w : E(G) \rightarrow S$ takie, że dla dowolnych
sąsiednich wierzchołków $u,v \in V(G)$, kolor wierzchołka $u$ jest różny od
koloru wierzchołka $v$.
\end{df}

Do tej pory nierozstrzygnięta jest następująca hipoteza sformułowana przez
Karońskiego, Łuczaka i Thomasona w pracy \cite{karonski}:

\begin{hip}\label{hip:colorable}
Każdy graf spójny $G$, $G \neq K_2$ jest wagowo kolorowalny przez zbiór
$\{1,2,3\}$.
\end{hip}
W tej samej pracy, autorzy udowodnili również, że istnieje wagowe kolorowanie dowolnym
zbiorem $S$ o liczności co najmniej 183, o ile $S$ jest zbiorem niezależnym nad
ciałem liczb wymiernych. Przy tym samym założeniu Addario-Berry, Aldred,
McDiarmid, Reed i Thomason pokazali (w \cite{addario}), że wystarczy zbiór o
liczności $4$.
Z kolei w pracy \cite{addario2} pokazano, że wystarczy zbiór trzydziestu liczb
naturalnych $\{1,2, \dots ,30 \}$. Warto zwrócić tutaj uwagę na to, że wynik ten
nie jest gorszy od tego uzyskanego w \cite{addario}, ponieważ tutaj
rezygnujemy z założenia o niezależności elementów danego zbioru użytych wag.
Obecnie cały czas próbuje się poprawić te osiągnięcia, jednak hipoteza jest wciąż nieudowodniona.
W niniejszej pracy będziemy korzystali z metody algebraicznej zaproponowanej
w artykule \cite{alon} przez Alona. Opiera się ona na twierdzeniu zwanym
Combinatorial Nullstellensatz.
Dzięki tej metodzie, ważenie krawędzi, czy też kolorowanie totalne grafów,
możemy sprowadzić do analizy pewnego wielomianu stowarzyszonego z grafem.
Twierdzenie to implikuje nawet mocniejszą tezę - wersję listową, tego samego
problemu. Zdefiniujmy zatem tę wersję problemu.

\begin{df}
Niech $G=(V,E)$, będzie grafem prostym oraz niech do każdej krawędzi tego
grafu będzie przypisany podzbiór liczb rzeczywistych $L_e$. \textit{Ważeniem z
list $(L_e)_{e \in E}$ } nazywamy ważenie krawędzi $w: E \rightarrow \bigcup_{e
\in E} L_e$, takie że dla każdej krawędzi $e \in E$, $w(e) \in L_e$.
\end{df}

\begin{df}
Graf $G$ jest \textit{wagowo wybieralny z list $(L_e)_{e \in E}$}, jeśli
istnieje ważenie z tych list, w którym każde dwa sąsiednie wierzchołki mają różne kolory.
\end{df}

\begin{df}
Graf $G$ jest \textit{$k$-wagowo wybieralny}, jeśli jest wagowo wybieralny z
dowolnych list długości $k$.
\end{df}

Teraz przedstawić możemy wersję listową hipotezy \ref{hip:colorable}:

\begin{hip}\label{hip:choosable}
Każdy graf bez izolowanej krawędzi jest $3$-wagowo wybieralny.
\end{hip}
Podobnie jak to ma miejsce w przypadku kolorowania wierzchołków i kolorowania z
list, jeżeli graf jest \textit{$k$-wagowo wybieralny} to jest również
\textit{wagowo kolorowalny} przez zbiór $\{1,2,\ldots,k\}$. 
Zatem udowodnienie hipotezy \ref{hip:choosable}, dowodzi również hipotezę
\ref{hip:colorable}. My udowodnimy hipotezę \ref{hip:choosable} dla pewnych
klas grafów oraz zaprezentujemy sposoby konstrukcji takich grafów.

Teraz przeanalizujmy przykład.

\begin{prz}
Rozważmy diament $D$, czyli cykl $C_4$ z dołożoną jedną cięciwą. Zauważmy, że
graf ten jest wagowo kolorowalny przez zbiór $\{1,2\}$, tak jak to widać na
rysunku \ref{fig:diament-zwazony}. \\ Z drugiej strony, graf ten nie jest wagowo
wybieralny z list takich jak na rysunku \ref{fig:diament-zlistami}.
Aby to uzasadnić, rozważmy na początku jakie możemy wybrać wagi, na krawędziach
$AB, BD, DC, CA$. Nie mogą to być same jedynki, ani też same dwójki,
ponieważ wtedy bez względu na to jaka będzie waga na krawędzi $AD$, to kolory
wierzchołków $A$ oraz $D$ będą takie same.
Jeśli na rozważanych krawędziach będą dwie jedynki oraz dwie dwójki, to jedynymi
ustawieniami, w których nie otrzymamy tych samych kolorów dla wierzchołków $A$
i $D$, będą ważenia, dla których obie jedynki będą na krawędziach po lewej stronie
(krawędzie $CA$ oraz $AB$), a dwójki po prawej stronie (krawędzie $BD$ oraz
$DC$) lub odwrotnie tzn. jedynki po prawej, dwójki po lewej. 
Ze względu na symetrię diamentu wystarczy, że rozważymy sytuację gdy jedynki są
z lewej a dwójki z prawej. 
Jeśli nadamy wagę $-1$ na krawędź $AD$, to wtedy $X_D=X_B$. Jeśli natomiast waga
$AD$ będzie równa jeden, to wtedy $X_A=X_B$.

Teraz rozważmy sytuację gdy na zewnętrznym cyklu w diamencie mamy trzy
jedynki i jedną dwójkę. Bez straty ogólności, dzięki symetrii diamentu,
wystarczy że rozpatrzymy sytuację gdy na krawędzi $DC$ mamy wagę $2$, a na
pozostałych krawędziach w cyklu mamy wagę równą jeden. Wtedy jeśli na krawędzi
$AD$ nadamy wagę $-1$, to otrzymamy $X_D=X_B$. Z kolei jeśli na cięciwie nadamy
wagę równą jeden, to $X_A=X_C$.

Rozumując analogicznie można pokazać, że jeśli na cyklu użyjemy dwójki
na trzech krawędziach i jedynki na czwartej, to również nie jesteśmy w stanie uzyskać
kolorowania właściwego.

W ten sposób pokazaliśmy, że diament nie jest wagowo wybieralny z list
przedstawionych na rysunku \ref{fig:diament-zlistami}.

Cały ten przykład potwierdza, że hipoteza \ref{hip:choosable} jest (w pewnym
sensie) istotnie mocniejsza od hipotezy \ref{hip:colorable}  a przynajmniej, że
parametr związany z wybieralnością z list może być istotnie większy niż w przypadku jego podstawowego odpowiednika.

\begin{figure}[h]

\begin{center}
\subfigure[Diament]{\includegraphics[width=0.25\textwidth]{images/diament-pusty.png}}
\subfigure[Diament zważony]{\label{fig:diament-zwazony}
\includegraphics[width=0.25\textwidth]{images/diament-zwazony.png}}
\subfigure[Diament
z przypisanymi
listami]{\includegraphics[width=0.25\textwidth]{images/diament-zlistami.png}
\label{fig:diament-zlistami}}
\caption{Przykład ważenia grafu.}
\end{center}
\end{figure}
\end{prz}


\newpage
\section{Wielomiany i permanent}
Niech $E=\{e_1,e_2,\ldots,e_m\}$ będzie zbiorem krawędzi grafu prostego $G$.
Niech $x_i$ będzie zmienną przypisaną do krawędzi $e_i$. Dla każdego wierzchołka
$u \in V(G)$, niech $E_u$ oznacza zbiór krawędzi incydentnych z $u$ oraz niech
$X_u = \sum_{e_j \in E_u} x_j$. Teraz ustalmy pewną orientację grafu $G$ i
zdefiniujmy wielomian $P_G$ zmiennych $x_1,\ldots,x_m$ jako:
\begin{equation}\label{def:wielomian}
P_G(x_1,x_2,\ldots,x_m)= \prod_{(u,v) \in E(G)} (X_v -X_u) .
\end{equation}
Zauważmy, że zmiana orientacji grafu $G$ może wpłynąć jedynie na znak powyższego
wielomianu. Znak wielomianu nie będzie dla nas istotny w dalszych rozważaniach,
zatem pisząc $P_G$ będziemy mieli na myśli jeden dowolnie wybrany wielomian z dwóch
możliwych.
\begin{obs}
Jeśli $P(s_1,\ldots,s_m) \neq 0$ dla pewnych $s_i \in S_i$, wtedy $G$ jest
wagowo wybieralny z list $(S_i)_{i\in \{1,2,\ldots,m\} }$.
\end{obs}

Niezwykle ważnym twierdzeniem, z którego będziemy korzystać, jest twierdzenie
sformułowane przez Alona:

\begin{tw}[Combinatorial Nullstellensatz] \label{tw:alon}
Niech $ \mathbb{F}$ będzie ustalonym ciałem i niech $P =P(x_1,\ldots,x_m)$
będzie wielomianem w $ \mathbb{F}[x_1,\ldots,x_m]$. Załóżmy, że \textit{stopień
wielomianu} $P$, $\deg(P)$, jest równy $ \sum_{i=1}^n k_i$, gdzie każde $k_i$
jest nieujemną liczbą całkowitą i załóżmy, że współczynnik przy jednomianie 
$x_1^{k_1}\dots x_m^{k_m}$ w $P$ jest niezerowy.

Wtedy, jeśli $S_1,\ldots S_m$ są podzbiorami zbioru $\mathbb{F}$ takimi, że
$|S_i|>k_i$, wówczas istnieją takie $s_1\in S_1,\ldots,s_m \in S_m$, dla których
$P(s_1,\ldots,s_m)\neq 0$.
\end{tw}
Do badania wagowej wybieralności grafu oraz stosowania powyższego twierdzenia,
będzie nam potrzebny pewien aparat pojęć. Aby je zdefiniować przyjmijmy, że $$
M=cx_1^{k_1} \dotsm x_m^{k_m}  ,$$
będzie jednomianem w rozwinięciu wielomianu $P$, gdzie $c\neq 0$.
Określamy następującą funkcję na zbiorze składającym się ze wszystkich
jednomianów $M$ (jak wyżej) występujących w rozwinięciu wielomianu $P$:
\begin{df}
$$h(M) :=\max_{i \in \{1,2,\ldots,m\}}{k_i}$$
\end{df}
Ponadto, użyteczna będzie następująca definicja:
\begin{df}
\textit{Indeksem jednomianowym (monomial index)} wielomianu $P$ nazywamy liczbę:
$$\mind(P) := \min_{M \in \mathcal{M}} h(M) ,$$
gdzie $ \mathcal{M}$ oznacza zbiór jednomianów występujących w rozwinięciu
wielomianu $P$ (z niezerowymi współczynnikami).
\end{df}
Przyjrzyjmy się teraz naszemu wielomianowi $P_G$, który opisaliśmy wcześniej.
Ponieważ dla różnych orientacji dostajemy ten sam wielomian, z
dokładnością do znaku, to wynika z tego że $\mind(P_G)$ jest niezmiennikem
grafu.
Od teraz mówiąc o \textit{indeksie jednomianowym  grafu} $G$, będziemy zatem
pisali po prostu $\mind(G)$, zamiast $\mind(P_G)$. Zauważmy, że $\mind(G)$ nie jest zdefiniowane
dla grafów z krawędzią izolowaną (wtedy $P_G\equiv 0$). W przeciwnym wypadku,
$P_G$ jest homogenicznym wielomianem stopnia $m$ oraz twierdzenie \ref{tw:alon}
implikuje następujący wniosek:
\begin{wn}
Jeśli $ \mind(G) \leqslant k$, wtedy $G$ jest $(k+1)$-wagowo wybieralny.
\end{wn}
Zatem, jeśli chcemy udowodnić wcześniej postawioną hipotezę, wystarczy pokazać,
że $\mind(G) \leqslant 2$ dla każdego grafu $G$. Na ogół trudno bada się indeks
jednomianowy, ale zamiast badania rozwinięcia tego wielomianu,
wystarczy zbadać permanent pewnej macierzy związanej z grafem $G$. 
\begin{df}
Niech $A=(a_{ij})$ będzie macierzą kwadratową rozmiaru $m$. \textit{Permanentem}
macierzy $A$ nazywamy liczbę:
$$ \per(A) = \sum_{\sigma \in S_m}
a_{1\sigma(1)}a_{2\sigma(2)}\dotsm a_{m\sigma(m)} $$
\end{df}
$S_m$ oznacza tutaj grupę permutacji zbioru $\{1,2,\ldots,m \}$.
Jak widać permanent jest bardzo podobny do wyznacznika - składników w sumach
jest tyle samo, różnią się one tylko tym, że w przypadku wyznacznika, są one
przemnożone przez znak permutacji. \\
Analogicznie jak w przypadku rzędu macierzy definiuje się \textit{rząd
permanentowy}.
\begin{df}
\textit{Rzędem permanentowym} macierzy $A$ nazywamy rozmiar największej
kwadratowej podmacierzy posiadającej permanent różny od zera.
\end{df}
Niech, $A^{(k)}=[A,A,\ldots,A]$ będzie macierzą powstałą z $k$ kopii macierzy
$A$.
\begin{df}
\textit{Indeksem permanentowym} macierzy kwadratowej $A$ nazywamy najmniejsze
$k$, takie, że $A^{(k)}$ ma rząd permanentowy równy rozmiarowi macierzy $A$.

Liczbę tą oznaczmy $\pind(A)$.

Jeśli takie $k$ nie istnieje, to przyjmujemy, że $\operatorname{pind}(A)=
\infty$.
\end{df}
Indeks permanentowy można zdefiniować też w inny sposób. Potrzebne do tego
będzie kilka oznaczeń. Niech $K=(k_1,k_2,\ldots,k_m)$ będzie dowolnym ciągiem liczb całkowitych
nieujemnych oraz niech $A$ będzie macierzą składającą się z kolumn
$A_1,A_2,\ldots A_m$. Oznaczmy przez $A(K)$ macierz powstałą z $A$ poprzez
powtórzenie $k_j$ razy każdej kolumny $A_j$. Wtedy \textit{indeks permanentowy}
możemy zdefiniować w następujący sposób:
\begin{df}
\textit{Indeks permanentowy} macierzy kwadratowej $A$ wymiaru $m$, jest to
najmniejsze $k$, takie, że $\per(A(K))\neq 0$ dla $K=(k_1,\ldots,k_m)$ przy czym
$K$ jest ciągiem takim że $k_1+k_2+\dotsm +k_m=m$, $k_i \in
\mathbb{N}\cup\{0\}$ oraz $k_i \leqslant k$ dla $i=1,\ldots,m$.
\end{df}
Łatwo zauważyć, że rzeczywiście te definicje są równoważne.

Przydatne będą następujące obserwacje dotyczące powyższych parametrów.
\begin{obs}\label{obs:transponowana}
Niech $A$ będzie macierzą kwadratową wymiaru $n$. Wtedy $\per(A)=\per(A^{T})$
\end{obs}
\begin{proof}

Aby to pokazać użyjmy następującego oznaczenia $A^T =[a^T_{ij}]$.
Wtedy \begin{align*}
\per(A^T) & =\sum_{\sigma  \in S_n} a^T_{1 \sigma(1)} a^T_{2
\sigma(2)} \dotsm a^T_{n \sigma(n)}
\end{align*} 
Ponieważ $a^T_{ij}=a_{ji}$, to otrzymujemy:
\begin{align*}
\per(A^T) =\sum_{\sigma  \in S_n} a_{\sigma(1)1} a_{\sigma(2) 2} \dotsm
a_{\sigma(n) n}.
\end{align*}
Zatem aby udowodnić równość $\per(A)=\per(A^T)$ wystarczy udowodnić następującą
równość:
$$ \sum_{\sigma  \in S_n} a_{\sigma(1)1} a_{\sigma(2) 2} \dotsm
a_{\sigma(n) n} = \sum_{\sigma  \in S_n} a_{1 \sigma(1)} a_{2 \sigma(2)}
\dotsm a_{n \sigma(n)} .$$

Ponieważ sumujemy po wszystkich permutacjach, to dla dowolnego ustalonego
składnika z lewej strony, generowanego przez permutację $\sigma_0$, istnieje
odpowiadający mu składnik o tej samej wartości po prawej stronie dowodzonej
równości (gdzie odpowiedniość ta jest wzajemnie jednoznaczna) - jest to składnik
odpowiadający permutacji $\sigma_0^{-1}$. Stąd wynika że obie sumy są sobie równe, co kończy dowód obserwacji.
\end{proof}

\begin{obs}
Niech $A$ będzie macierzą kwadratową wymiaru $n$, składającą się z kolumn
$A_1,A_2,\ldots,A_n$. Niech $A_{ij}$ oznacza $j$-ty element kolumny $A_i$. Wtedy
permanent możemy równoważnie zdefiniować jako $$ \per(A_1,A_2,\ldots,A_n) = \sum_{\sigma \in S_n}
A_{1\sigma(1)}A_{2\sigma(2)},\ldots,A_{n\sigma(n)} .$$
Analogicznie możemy zdefiniować takie odwzorowanie dla wierszy macierzy $A$.
\end{obs}

Łatwo zauważyć, że tak określone odwzorowania są odwzorowaniami wieloliniowymi.

Niezwykle pomocny będzie następujący lemat:
\begin{lem}\label{lem:mind-rowny-pind}
Niech $A=(a_{ij})$ będzie macierzą kwadratową rozmiaru $m$ o skończonym indeksie
permanentowym. \\
Niech $P(x_1,x_2,\ldots,x_m) = \prod_{i=1}^{m} (a_{i1}x_1 + \dotsm +
a_{im}x_m)$.
\\
Wtedy $\mind(P) = \pind(A)$.
\end{lem}
\begin{proof}
Aby udowodnić to twierdzenie pokażemy, że współczynnik przy jednomianie
$x_{1}^{k_1}\cdots x_{m}^{k_m}$ w rozwinięciu wielomianu $
P(x_1,x_2,\ldots,x_m) = \prod_{i=1}^{m} (a_{i1}x_1 + \dotsm + a_{im}x_m)$ jest równy
$\frac{\per(A(K))}{k_1!\cdots k_m!}$, gdzie $K=(k_1,\ldots,k_m)$. Rozpiszmy zatem badany wielomian.
\begin{equation}\label{eq:Polynomial}
\begin{split}
P(x_1,x_2,\ldots ,x_m) & = (a_{11}x_1 +a_{12}x_2 + \dotsm +
a_{1m}x_m) \cdot \\
 & \cdot (a_{21}x_1 +a_{22}x_2 + \dotsm +
a_{2m}x_m) \cdot \\
 & \vdots \\
 & \cdot (a_{m1}x_1 +a_{m2}x_2 + \dotsm +
a_{mm}x_m).
\end{split}
\end{equation}
Rozważmy teraz macierz $A(K)$. Jest ona postaci
$$A(K) = \begin{bmatrix}
a_{11} & \cdots & a_{11} & a_{12}  & \cdots & a_{12} & \cdots &
a_{1 m} & \cdots & a_{1 m} \\
a_{21} & \cdots & a_{21} & a_{22}  & \cdots & a_{22} & \cdots &
a_{2 m} & \cdots & a_{2 m} \\
\vdots & \ddots & \vdots & \vdots & \ddots & \vdots & \ddots & \vdots & \ddots &
\vdots \\
a_{m1} & \cdots & a_{m1} & a_{m2}  & \cdots & a_{m2} & \cdots &
a_{m m} & \cdots & a_{m m}
 \end{bmatrix}. $$ 
 Z dowodu obserwacji \ref{obs:transponowana} wynika że prawdziwe jest równanie
 $$\per(A) = \sum_{\sigma \in S_m}
a_{1\sigma(1)}a_{2\sigma(2)}\dotsm a_{m\sigma(m)} = \sum_{\sigma \in S_m}
a_{\sigma(1) 1}a_{\sigma(2) 2}\dotsm a_{\sigma(m) m} .$$
Policzmy zatem $\per(A(K))$.
\begin{equation} \label{eq:perLemma}
\per(A(K)) =   \sum_{\sigma \in S_m}
a_{\sigma(1) 1} \dotsm a_{\sigma(k_1) 1} \cdot a_{\sigma(k_1+1)
2} \dotsm a_{\sigma(k_1+k_2) 2} \dotsm a_{\sigma(k_1+ \dotsm
k_{m-1}+1) m} \dotsm a_{m m}.
\end{equation}
W dowodzie przydatna będzie następująca funkcja:
\begin{df}
Niech $S=\{s_1,\ldots,s_n\}$ będzie skończonym podzbiorem zbioru liczb
naturalnych.
Dla takich podzbiorów definiujemy funkcję:  $$I(S)=s_1\cdot s_2 \cdots s_n .$$
Przyjmijmy ponadto, że $I(\emptyset)=1$.
\end{df}
Zauważmy, że przy tak zdefiniowanej funkcji $I$ 
z równania (\ref{eq:perLemma}) mamy
\begin{displaymath}
\per(A(K)) = \sum_{\sigma \in S_m}
I(\{a_{\sigma(1) 1}, \ldots , a_{\sigma(k_1) 1} \}) \dotsm I(\{a_{\sigma(k_1+
\dotsm k_{m-1}+1) m}, \ldots , a_{\sigma(m) m}\}).
\end{displaymath}
Dla uproszczenia zapisu przyjmijmy następujące oznaczenie
$$ I(\sigma) = I(\{a_{\sigma(1) 1}, \ldots , a_{\sigma(k_1) 1} \}) \dotsm I(\{a_{\sigma(k_1+
\dotsm k_{m-1}+1) m}, \ldots , a_{\sigma(m) m}\}), $$
gdzie $\sigma$ jest permutacją ze zbioru $S_m$. Czyli 
\begin{equation} \label{eq:perLemma2}
 \per(A(K)) =   \sum_{\sigma \in S_m} I(\sigma). 
 \end{equation}
W zbiorze wszystkich permutacji $S_m$ możemy wprowadzić następującą relację $R$:
$$\sigma R \phi \iff \begin{cases}
\{ \sigma(1), \sigma(2),\ldots, \sigma(k_1) \} = \{ \phi(1), \phi(2),\ldots,
\phi(k_1) \} \\
\vdots \\
\{ \sigma(k_1+\cdots + k_{m-1}+1), \ldots, \sigma(m) \} = \{
\phi(k_1+\cdots + k_{m-1}+1),\ldots, \phi(m) \}
 \end{cases}$$
 Widać, że jest to relacja równoważności. Relacja ta dzieli również zbiór
 wszystkich permutacji na klasy równoważności o liczności $k_1! \cdots k_m!$.
 Dla dowolnego reprezentanta $\sigma$ klasy $[\sigma]$ iloczyn $I(\sigma)$
jest taki sam, zatem z równania (\ref{eq:perLemma2}) mamy:
$$\per(A(K)) = \sum_{[\sigma] \in S_m /R} I(\sigma) \cdot k_1!\cdots k_m!  .$$
Zatem, aby udowodnić lemat, wystarczy pokazać, że liczba $ \sum_{[\sigma]
\in S_m /R} I(\sigma) $ jest równa współczynnikowi przy jednomianie
$x_{1}^{k_1}\cdots x_{m}^{k_m}$.
Rzeczywiście tak jest. Aby to pokazać, przyjrzyjmy się rozważanemu wielomianowi
(\ref{eq:Polynomial}). Zastanówmy się jak możemy obliczyć współczynnik przy
interesującym nas jednomianie. Ponieważ badany wielomian jest w postaci
iloczynowej, to w celu otrzymania konkretnego współczynnika, musimy wybrać $k_1$ nawiasów z
których do iloczynu weźmiemy składniki zawierające $x_1$, $k_2$ nawiasów spośród
pozostałych, z których do iloczynu weźmiemy składniki zawierające $x_2$, itd.,
aż do $x_m$, dzięki czemu otrzymamy żądany jednomian. Każdemu takiemu wyborowi
odpowiada dokładnie jedna klasa równoważności $[\sigma] \in S_{m}/R$.
Aby otrzymać żądany współczynnik musimy zsumować po wszystkich takich możliwych
wyborach, czyli klasach abstrakcji. Pokazaliśmy zatem, że liczba $ \sum_{[\sigma]
\in S_m /R} I(\sigma) $ jest równa współczynnikowi przy jednomianie
$x_{1}^{k_1}\cdots x_{m}^{k_m}$, co kończy dowód całego lematu.
\end{proof}
\begin{df}
Niech $G$ będzie grafem o ustalonej orientacji. Macierz $A_G=(a_{ij})$
definiujemy w następujący sposób:
$$ a_{ij} =   \begin{cases}
     1  &  \mathrm{jeśli}\ e_j\ \mbox{jest incydentne z końcem }\
     e_i
     \\
     -1  &  \mathrm{jeśli}\ e_j\ \mbox{jest incydentne z początkiem }\
     e_i \\
     0  &  \mathrm{jeśli}\ e_i\ \mathrm{i}\ e_j\ \mbox{nie są incydentne}
  \end{cases} $$
\end{df}
Łatwo przyswoić sobie tę definicję patrząc na rysunki \ref{rys:macierzAG}.
\begin{figure}[h]
\caption{Definicja macierzy $A_G$}\label{rys:macierzAG}
\begin{center}
\subfigure[Łuk $(u,v)$
]{\includegraphics[width=0.25\textwidth]{images/luk-oznaczenia.png}}
\subfigure[$a_{ij}=1$]{\includegraphics[width=0.25\textwidth]{images/luk-ej-incydentne-z-koncem-ei.png}}
\subfigure[$a_{ij}=-1$]{
\includegraphics[width=0.25\textwidth]{images/luk-ej-incydentne-z-poczatkiem-ei.png}}
\end{center}
\end{figure}

Dla tak określonej macierzy $A_G$, prawdziwa jest następująca obserwacja: 
\begin{obs}\label{obs:pg-rowny-A}
\begin{equation} \label{eq:polynomialmatrix} 
P_G(x_1,x_2, \ldots , x_m) =
\prod_{i=1}^m (a_{i1}x_1 + \dotsm + a_{im} x_m). 
\end{equation}
\end{obs}
\begin{proof}
Rzeczywiście, porównajmy czynniki odpowiadające dowolnemu, ustalonemu łukowi
$e_{i_0}$. Ponieważ rozważamy czynnik odpowiadający tylko tej krawędzi, to
orientacja pozostałych krawędzi nie zmienia żadnej ze stron rozważanej równości,
dlatego bez straty ogólności możemy przyjąć, że wszystkie łuki oprócz $e_{i_0}$
są skierowane do wierzchołka $u$ lub $v$.
\begin{figure}[h]
\begin{center}
\includegraphics[width=\textwidth]{images/luk-ei0-ogolny-przypadek.png}
\end{center}
\caption{Łuk $e_{i_0}$}
\end{figure}
Czynnik odpowiadający $e_{i_0}$ po lewej stronie równania
(\ref{eq:polynomialmatrix}) (oznaczmy go jako $L_{e_{i_0}}$), zgodnie z
definicją (\ref{def:wielomian}) wynosi $X_v -X_u$, czyli
 $$ L_{e_{i_0}} = (\sum_{i=1}^{q} x_{v_i} + \sum_{i=1}^{r} x'_{uv_i} ) -
 (\sum_{i=1}^{p} x_{u_i} + \sum_{i=1}^{r} x_{uv_i} ) .$$
 Z kolei odpowiedni czynnik po prawej stronie (oznaczmy go jako $P_{e_{i_0}}$),
 wynosi
 $$ P_{e_{i_0}} = \sum_{i=1}^{q} 1\cdot x_{v_i} + \sum_{i=1}^{r} 1\cdot
 x'_{uv_i} + \sum_{i=1}^{p} (-1)\cdot x_{u_i} + \sum_{i=1}^{r} (-1)\cdot
 x_{uv_i} .$$ Stąd łatwo zauważyć, że $L_{e_{i_0}}=P_{e_{i_0}}$, dla każdego
 czynnika, zatem otrzymujemy oczekiwaną równość.
 \end{proof}
 Dzięki tej obserwacji oraz lematowi \ref{lem:mind-rowny-pind} mamy:
 \begin{equation}
 \mind(G)=\pind(A_G).
 \end{equation}
 Stąd otrzymujemy następujący wniosek:
\begin{wn}
Jeśli $\pind(A_G) \leqslant k$, wtedy $G$ jest $k+1$-wagowo wybieralny. W
szczególności jeśli $\per(A_G) \neq 0$, to $G$ jest $2$-wybieralny.
\end{wn}
Zobaczmy przykład zastosowania powyżej poznanych pojęć i twierdzeń.
\begin{figure}[h] 
\begin{center}
\includegraphics[width=0.5\textwidth]{images/diament-zorientowany.png}
\end{center}
\caption{Zorientowany diament}\label{rys:zorientowany-diament}
\end{figure}
\begin{prz}

Zorientujmy diament $D$ tak jak na rysunku \ref{rys:zorientowany-diament}.

Dla tak zorientowanego grafu otrzymujemy:

$$ P_D =
(x_2-x_4-x_5)(x_3+x_5-x_1)(x_4-x_2-x_5)(x_1+x_5-x_3)(x_1+x_4-x_2-x_3).$$
 Macierz incydencji posiada $5$ wierszy odpowiadających składnikom
wielomianu: $$A_D = \begin{bmatrix}
0 & 1 & 0 & -1 & -1 \\ 
-1 & 0 & 1 & 0 & 1\\ 
0 & -1 & 0 & 1 & -1\\ 
1 & 0 & -1 & 0 & 1\\ 
1 & -1 & -1 & 1 & 0
\end{bmatrix}. $$

Można wyliczyć, że $\mind(D)=2$.

Rzeczywiście: \\
$\per(A_D) =0$, ale dla $K=(1,1,1,2,0)$ otrzymujemy macierz:

$$A_D(K) = \begin{bmatrix}
0 & 1 & 0 & -1 & -1 \\ 
-1 & 0 & 1 & 0 & 0\\ 
0 & -1 & 0 & 1 & 1\\ 
1 & 0 & -1 & 0 & 0\\ 
1 & -1 & -1 & 1 & 1
\end{bmatrix}. $$

dla której $\per(A_D(K))=12$
\footnote{Liczenie permenentu jest dość żmudne. Można go szybko wyliczyć w
odpowiednim pakiecie matematycznym, takim jak np. Maple. Więcej informacji można znaleźć w dokumentacji
\cite{maple-permanent}.}\\


 Stąd wynika, że $D$ jest $3$-wybieralny.

\end{prz}



